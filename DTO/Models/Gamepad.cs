﻿namespace DTO.Models
{
    public class JoystickState
    {
        public int Z { get; }
        public int RotationX { get; }
        public int RotationY { get; }
        public int RotationZ { get; }
        public int VelocityX { get; }
        public int VelocityY { get; }
        public int VelocityZ { get; }
        public int AngularVelocityX { get; }
        public int AngularVelocityY { get; }
        public int AngularVelocityZ { get; }
        public int AccelerationX { get; }
        public int AccelerationY { get; }
        public int AccelerationZ { get; }
        public int AngularAccelerationX { get; }
        public int Y { get; }
        public int AngularAccelerationY { get; }
        public int ForceX { get; }
        public int ForceY { get; }
        public int ForceZ { get; }
        public int TorqueX { get; }
        public int TorqueY { get; }
        public int TorqueZ { get; }
        public int AngularAccelerationZ { get; }
        public int X { get; }
    }
}
