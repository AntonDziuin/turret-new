﻿using SharpDX.DirectInput;
using System;
using System.Threading;
using System.Windows;

namespace JoystickLib
{
    public class GamepadObj
    {
        private Joystick joystick;
        private Guid joystickGuid;
        private DirectInput directInput;
        Thread thread;
        public Action<JoystickUpdate[]> catchUpdate;
        public GamepadObj()
        {
            Start();
        }

        public void Start()
        {
            thread = new Thread(() => Listen());
            thread.Name = "Joystick Thread";
            directInput = new DirectInput();
            joystickGuid = Guid.Empty;

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad,
                        DeviceEnumerationFlags.AllDevices))
                joystickGuid = deviceInstance.InstanceGuid;

            if (joystickGuid == Guid.Empty)
                foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick,
                        DeviceEnumerationFlags.AllDevices))
                    joystickGuid = deviceInstance.InstanceGuid;

            if (joystickGuid == Guid.Empty)
            {
                MessageBox.Show("No joystick/Gamepad found.");
            }

            joystick = new Joystick(directInput, joystickGuid);
            joystick.Properties.BufferSize = 128;
            joystick.Acquire();
            thread.Start();
        }

        private void Listen()
        {
            while (true)
            {
                joystick.Poll();
                var datas = joystick.GetBufferedData();
                catchUpdate(datas);
            }
        }
    }
}
