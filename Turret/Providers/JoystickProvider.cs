﻿using JoystickLib;
using SharpDX.DirectInput;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;
using Turret.Interfaces;
using Turret.Models;

namespace Turret.Providers
{
    class JoystickProvider : IGamepad
    {
        private const int timerInterval = 40;
        private const int joystickIndex = 0;
        private GamepadObj gamepad;

        private Models.JoystickState joystickState;

        public JoystickProvider(){
            InitPer();
        }

        private void InitPer()
        {
            joystickState = new Models.JoystickState();
            joystickState.axisX.minValue = 0;
            joystickState.axisX.maxValue = ushort.MaxValue;
            joystickState.axisY.minValue = 0;
            joystickState.axisY.maxValue = ushort.MaxValue;
            joystickState.zoomPlus.minValue = 0;
            joystickState.zoomPlus.maxValue = 128;
            joystickState.zoomMinus.minValue = 0;
            joystickState.zoomMinus.maxValue = 128;
            joystickState.focusPlus.minValue = 0;
            joystickState.focusPlus.maxValue = 128;
            joystickState.focusMinus.minValue = 0;
            joystickState.focusMinus.maxValue = 128;
            gamepad = new GamepadObj
            {
                catchUpdate = s => Process(s)
            };
        }

        int iter = 0;
        private void Process(JoystickUpdate[] s)
        {
            for(iter = 0; iter < s.Length; iter++)
            {
                Debug.WriteLine(s[iter]);
                switch (s[iter].Offset)// это название канала
                {
                    case JoystickOffset.X:
                        joystickState.axisX.currentValue = s[iter].Value;
                        break;
                    case JoystickOffset.Y:
                        joystickState.axisY.currentValue = s[iter].Value;
                        break;
                    case JoystickOffset.Buttons4:
                        joystickState.zoomPlus.currentValue = s[iter].Value;
                        break;
                    case JoystickOffset.Buttons6:
                        joystickState.zoomMinus.currentValue = s[iter].Value;
                        break;
                    case JoystickOffset.Buttons5:
                        joystickState.focusPlus.currentValue = s[iter].Value;
                        break;
                    case JoystickOffset.Buttons7:
                        joystickState.focusMinus.currentValue = s[iter].Value;
                        break;
                    default:
                        break;
                }
            }
        }
        
        public Channel axisX()
        {
            return joystickState.axisX;
        }

        public Channel axisY()
        {
            return joystickState.axisY;
        }

        public Channel zoomPlus()
        {
            return joystickState.zoomPlus;
        }

        public Channel zoomMinus()
        {
            return joystickState.zoomMinus;
        }

        public Channel focusPlus()
        {
            return joystickState.focusPlus;
        }

        public Channel focusMinus()
        {
            return joystickState.focusMinus;
        }
    }
}
