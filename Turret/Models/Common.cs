﻿namespace Turret.Models
{
    public class Channel
    {
        public string name { get; set; } = "";
        public int minValue { get; set; }
        public int maxValue { get; set; }
        public int currentValue { get; set; }
        public int address { get; set; }
    }

    public class JoystickState
    {
        public Channel axisX { get; set; } = new Channel();
        public Channel axisY { get; set; } = new Channel();
        public Channel zoomPlus { get; set; } = new Channel();
        public Channel zoomMinus { get; set; } = new Channel();
        public Channel focusPlus { get; set; } = new Channel();
        public Channel focusMinus { get; set; } = new Channel();
    }
}
