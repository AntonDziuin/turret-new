﻿using Turret.Models;

namespace Turret.Interfaces
{
    public interface IGamepad
    {
        Channel axisX();
        Channel axisY();
        Channel zoomPlus();
        Channel zoomMinus();
        Channel focusPlus();
        Channel focusMinus();
    }
}
