﻿using System.Windows;
using Turret.Providers;
namespace Turret
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        JoystickProvider joystick;
        public MainWindow()
        {
            InitializeComponent();
            Init();
        }
        private void Init()
        {
            joystick = new JoystickProvider();
        }
    }
}
